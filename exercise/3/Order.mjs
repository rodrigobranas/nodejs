export default class Order {
	constructor () {
		this.number = Math.floor(Math.random() * 1000);
		this.date = new Date();
		this.orderItems = [];
	}

	addOrderItem(item) {
		this.orderItems.push(item);
	}

	get total() {
		let total = 0;
		for (const orderItem of this.orderItems) {
			total += orderItem.price;
		}
		return total;
	}
}
