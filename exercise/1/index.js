class Item {
	constructor (description, price) {
		this.description = description;
		this.price = price;
	}
}

class Order {
	constructor () {
		this.number = Math.floor(Math.random() * 1000);
		this.date = new Date();
		this.orderItems = [];
	}

	addOrderItem(item) {
		this.orderItems.push(item);
	}

	get total() {
		let total = 0;
		for (const orderItem of this.orderItems) {
			total += orderItem.price;
		}
		return total;
	}
}

const bola = new Item('Bola', 20);
const raquete = new Item('Raquete', 200);
const order = new Order();
order.addOrderItem(bola);
order.addOrderItem(bola);
order.addOrderItem(bola);
order.addOrderItem(raquete);
console.log(order);
console.log(order.total);
