function _Book (title, author, year) {
	this.title = title;
	this.author = author;
	this.year = year;
}

_Book.prototype.print = function () {
	console.log(this.title, this.author, this.year);
}

class Book {
	constructor(title, author, year) {
		this.title = title;
		this.author = author;
		this.year = year;
	}

	print() {
		console.log(this.title, this.author, this.year)
	}
}

function _new (fn, ...params) {
	const obj = {};
	Object.setPrototypeOf(obj, fn.prototype);
	fn.apply(obj, params);
	return obj;
}

const book = new Book('Clean Code', 'Robert C. Martin', '2009');
book.print();
const _book = _new(_Book, 'Clean Code', 'Robert C. Martin', '2009');
_book.print();
