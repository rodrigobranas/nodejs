const { Worker, isMaster, fork } = require('cluster');
const http = require('http');
const os = require('os');
const fibonacci = require('./fibonacci');

const availableCPUs = os.cpus(); // 8

if (isMaster) {
	console.log('master', process.pid);
	let total = 0;
	for (const cpu of availableCPUs) {
		const worker = fork();
		worker.on('message', function (message) {
			console.log(message);
			total += message.result;
			console.log(total);
		});
	}
} else {
	console.log('worker', process.pid);
	http.createServer(function (req, res) {
		res.writeHead(200);
		const result = fibonacci(40);
		process.send({pid: process.pid, result});
		res.write(result.toString());
		res.end();
	
	}).listen(8000);
}
