const { Worker, isMainThread, parentPort, workerData } = require('worker_threads');
const fibonacci = require('./fibonacci');

if (isMainThread) {
	console.log('main');
	const handler = function (message) {
		console.log(message);
	};
	const a = new Worker(__filename, { workerData: { code: 'a', number: 45 } });
	a.on('message', handler);
	const b = new Worker(__filename, { workerData: { code: 'b', number: 45 } });
	b.on('message', handler);
	const c = new Worker(__filename, { workerData: { code: 'c', number: 45 } });
	c.on('message', handler);
} else {
	console.log(`thread ${workerData.code} ${workerData.number}`);
	const result = fibonacci(workerData.number);
	parentPort.postMessage({ code: workerData.code, result });
}
