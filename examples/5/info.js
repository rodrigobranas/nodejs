const help = [
	'a) pid', 
	'b) title', 
	'c) arch', 
	'd) platform'
];

function showOptions () {
	for (const value of help) {
		console.log(value);
	}
}

const options = process.argv.slice(2);

if (options.length < 1) {
  showOptions();
  return;
}

const commands = {
	a: 'pid',
	b: 'title',
	c: 'arch',
	d: 'platform'
};

const [option] = options;
console.log(process[commands[option]]);
