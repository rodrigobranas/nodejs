const http = require('http');
const fibonacci = require('./fibonacci');

http.createServer(function (req, res) {
	res.writeHead(200);
	const result = fibonacci(45);
	res.write(result.toString());
	res.end();

}).listen(8000);
