const max = Math.floor(Math.random() * 1000);

const generate = function () {
	return Math.floor(Math.random() * max);
};

module.exports = {
	max,
	generate
};
