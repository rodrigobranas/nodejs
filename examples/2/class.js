class Book {
	constructor (title) {
		this.title = title;
	}
}

const book = new Book('Clean Code');
console.log(book);
