var help = [
	'a) pid', 
	'b) title', 
	'c) arch', 
	'd) platform'
];

const showOptions = function () {
 	help.forEach(function (value) {
		console.log(value);
	});
};

var options = process.argv.slice(2);
if (options.length < 1) {
  showOptions();
  return;
}
var option = options[0];
switch(option) {
	case 'a': 
	  console.log('pid: ' + process.pid);
	  break;
	case 'b':
	  console.log('title: ' + process.title);
	  break;
	case 'c':
	  console.log('arch: ' + process.arch);
	  break;
	case 'd':
	  console.log('platform: ' + process.platform);
	  break;
	default:
	  showOptions();
}