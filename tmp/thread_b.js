const { Worker, isMainThread, parentPort, workerData } = require('worker_threads');
const fibonacci = require('./fibonacci');

if (isMainThread) {
	console.log('main');
	const a = new Worker(__filename, { workerData: { code: 'a', number: 45 } });
	a.on('message', function (message) {
		console.log(message);
	});
	const b = new Worker(__filename, { workerData: { code: 'b', number: 45 } });
	b.on('message', function (message) {
		console.log(message);
	})
	const c = new Worker(__filename, { workerData: { code: 'c', number: 45 } });
	c.on('message', function (message) {
		console.log(message);
	});
} else {
	if (!workerData) return;
	console.log('thread', workerData.code, workerData.number);
	const result = fibonacci(workerData.number);
	parentPort.postMessage(`thread ${workerData.code} ${result}`);
}
