const { Worker, isMainThread, workerData } = require('worker_threads');
const loop = require('./loop');

if (isMainThread) {
	console.log('main');
	new Worker(__filename, { workerData: { code: 'a', number: 100000 } });
	new Worker(__filename, { workerData: { code: 'b', number: 100000 } });
	new Worker(__filename, { workerData: { code: 'c', number: 100000 } });
} else {
	if (!workerData) return;
	console.log('thread', workerData.code, workerData.number);
	loop(workerData.code, workerData.number);
}
